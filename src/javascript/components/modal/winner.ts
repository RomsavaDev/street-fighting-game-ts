import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import {fighterType} from '../../services/fightersService';

export function showWinnerModal(fighter:fighterType) {
  const winElement: HTMLElement = createElement({
    tagName: 'span',
    className: 'modal-body',
  });
  winElement.innerText = fighter.name;

  showModal({ title: 'WINNER IS', bodyElement: winElement });

  // call showModal function
  //!done
}
