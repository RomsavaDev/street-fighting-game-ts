import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import {fighterType} from '../services/fightersService';

export function renderArena(selectedFighters:[fighterType,fighterType]) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);


    fight(...selectedFighters).then((item:fighterType) => {
      showWinnerModal(item);
    });

  // todo:
  // - start the fight
  // - when fight is finished show winner

  // Done
}

function createArena(selectedFighters:[fighterType,fighterType]) {
  const arena: HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators: HTMLElement = createHealthIndicators(...selectedFighters);
  const fighters: HTMLElement = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter:fighterType, rightFighter:fighterType) {
  const healthIndicators: HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign: HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator: HTMLElement = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator: HTMLElement = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter:fighterType, position: 'left' | 'right') {
  const { name } = fighter;
  const container: HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName: HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` },
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter:fighterType, secondFighter:fighterType) {
  const battleField: HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement: HTMLElement = createFighter(firstFighter, 'left');
  const secondFighterElement: HTMLElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter:fighterType, position: 'left' | 'right') {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
