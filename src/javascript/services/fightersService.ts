import { callApi } from '../helpers/apiHelper';


export type fighterType = {
  [index:string]:any
  _id: string;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;
}



class FighterService {
  async getFighters() : Promise<fighterType[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult = <fighterType[]> await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');
      
      return apiResult;
    } catch (error) {
      throw error;
    }


    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
  }
}

export const fighterService = new FighterService();
